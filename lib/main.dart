import 'package:flutter/material.dart';

void main() {
  runApp(Nav2App());
}

class Nav2App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: (settings) {
        // Handle '/'
        if (settings.name == '/') {
          return MaterialPageRoute(builder: (context) => HomeScreen());
        }

        // Handle '/details/:id'
        var uri = Uri.parse(settings.name);
        if (uri.pathSegments.length == 2 &&
            uri.pathSegments.first == 'details') {
          var id = uri.pathSegments[1];
          return MaterialPageRoute(builder: (context) => DetailScreen(id: id));
        }

        return MaterialPageRoute(builder: (context) => UnknownScreen());
      },
    );
  }
}

class HomeScreen extends StatelessWidget {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFFF53C16),
          title: Text('KG-MOTOCYCLE'),
        ),
        body: ListView(children: [
          Image.asset(
            'images/001.jpg',
            width: 700,
            height: 500,
          ),
          Text(
            'KG MOTOCYCLE',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            'ซื้อขาย มอเตอร์ไซต์ศูนย์รวมมอเตอร์ไซต์',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.grey[5000],
            ),
          ),
          Center(
            child: FlatButton(
              child: Text('จองรถที่นี่'),
              color: Color(0xFFF53C16),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/details/1',
                );
              },
            ),
          ),
        ]));
  }
}

class DetailScreen extends StatelessWidget {
  String id;

  DetailScreen({
    this.id,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF0B42FF),
        title: Text('KG-MOTOCYCLE'),
      ),
      body: ListView(children: [
        Image.asset(
          'images/001.jpg',
          width: 900,
          height: 400,
        ),
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: [
                  Column(
                    children: [
                      Image.asset(
                        'images/100.jpg',
                        width: 300,
                        height: 500,
                      ),
                      Text('Wave 100 '),
                      Text('ราคา 31,000 บาท'),
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset(
                        'images/110.jpg',
                        width: 300,
                        height: 500,
                      ),
                      Text('Wave i 110 '),
                      Text('ราคา 45,200 บาท'),
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset(
                        'images/002.jpg',
                        width: 300,
                        height: 500,
                      ),
                      Text('Wave 125i '),
                      Text('ราคา 55,600'),
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset(
                        'images/scoopy.jpg',
                        width: 300,
                        height: 500,
                      ),
                      Text('Super Cub '),
                      Text('ราคา 47,400 บาท'),
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset(
                        'images/super.jpg',
                        width: 300,
                        height: 500,
                      ),
                      Text('Honda Scoopy i '),
                      Text('ราคา 48,900 บาท'),
                    ],
                  ),
                ],
              ),
              Text(''),
              FlatButton(
                child: Text('กลับหน้าหลัก'),
                color: Color(0xFF0B42FF),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              Row(
                children: [
                  Icon(Icons.face_sharp, color: Colors.red[800], size: 70),
                  Text('KG MOTOCYCLE:'),
                  Icon(Icons.call, color: Colors.green[500], size: 50),
                  Text('0848437204:'),
                ],
              ),
            ],
          ),
        ),
      ]),
    );
  }
}

class UnknownScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Text('404!'),
      ),
    );
  }
}
